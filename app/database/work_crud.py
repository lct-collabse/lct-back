from sqlalchemy.orm import Session
from app.schemas.work import WorkCreate
from app.database import crud_base

class WorkCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_work(self, db: Session, work: WorkCreate):
        work_data = {
            "user_id": work.user_id,
            "company_id": work.company_id,
            "start_date": work.start_date,
            "end_date": work.end_date,
            "job_title": work.job_title,
            "job_description": work.job_description,
        }
        return self.create(db, **work_data)

    def update_work(self, db: Session, work_id: int, work: WorkCreate):
        work_data = {
            "company_id": work.company_id,
            "start_date": work.start_date,
            "end_date": work.end_date,
            "job_title": work.job_title,
            "job_description": work.job_description,
        }
        return self.update(db, work_id, **work_data)