from sqlalchemy.orm import Session
from app.schemas.user import UserCreate
from app.database import crud_base

class UserCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_user(self, db: Session, user: UserCreate):
        user_data = {
            "first_name": user.first_name,
            "middle_name": user.middle_name,
            "last_name": user.last_name,
            "email": user.email,
            "vk_id": user.vk_id,
            "tg_id": user.tg_id,
            "password": user.password,
            "phone_number": user.phone_number,
            "role": user.role
        }
        return self.create(db, **user_data)
    
    def update_user(self, user_id: int, user: UserCreate, db: Session):
        user_data = {
            "first_name": user.first_name,
            "middle_name": user.middle_name,
            "last_name": user.last_name,
            "email": user.email,
            "vk_id": user.vk_id,
            "tg_id": user.tg_id,
            "password": user.password,
            "phone_number": user.phone_number,
            "role": user.role
        }
        return self.update(user_id, db, **user_data)

    def get_by_email(self, db: Session, email: str):
        return db.query(self.model).filter(self.model.email == email).first()
    
    def validate_user(self, db: Session, email: str, password: str):
        found_user = db.query(self.model).filter(self.model.email == email).first()
        
        if found_user is None:
            return None
        if found_user.password != password:
            return None
        return found_user