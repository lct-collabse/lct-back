from sqlalchemy.orm import Session
from app.schemas.event_record import EventRecordCreate
from app.database import crud_base

class EventRecordCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_event_record(self, db: Session, event_record: EventRecordCreate):
        event_record_data = {
            "participant_id": event_record.participant_id,
            "event_id": event_record.event_id,
            "internship_id": event_record.internship_id,
        }
        return self.create(db, **event_record_data)
    
    def update_event_record(self, db: Session, event_record_id: int, event_record: EventRecordCreate):
        event_record_data = {
            "participant_id": event_record.participant_id,
            "event_id": event_record.event_id,
            "internship_id": event_record.internship_id,
        }
        return self.update(db, event_record_id, **event_record_data)