from sqlalchemy.orm import Session
from app.schemas.review import ReviewCreate
from app.database import crud_base

class ReviewCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_review(self, db: Session, review: ReviewCreate):
        review_data = {
            "author_id": review.author_id,
            "target_id": review.target_id,
            "target_type": review.target_type,
            "name": review.name,
            "description": review.description,
            "grade": review.grade,
        }
        return self.create(db, **review_data)

    def update_review(self, db: Session, review_id: int, review: ReviewCreate):
        review_data = {
            "target_id": review.target_id,
            "target_type": review.target_type,
            "name": review.name,
            "description": review.description,
            "grade": review.grade,
        }
        return self.update(db, review_id, **review_data)
