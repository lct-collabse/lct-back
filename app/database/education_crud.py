from sqlalchemy.orm import Session
from app.schemas.education import EducationCreate
from app.database.crud_base import CRUDBase

class EducationCRUD(CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_education(self, db: Session, education: EducationCreate):
        education_data = {
            "user_id": education.user_id,
            "university_id": education.university_id,
            "start_date": education.start_date,
            "end_date": education.end_date,
            "speciality_title": education.speciality_title,
            "education_level": education.education_level,
        }
        return self.create(db, **education_data)
    
    def update_education(self, db: Session, education_id: int, education: EducationCreate):
        education_data = {
            "university_id": education.university_id,
            "start_date": education.start_date,
            "end_date": education.end_date,
            "speciality_title": education.speciality_title,
            "education_level": education.education_level,
        }
        return self.update(db, education_id, **education_data)