from sqlalchemy.orm import Session
from app.schemas.event_entity import EventEntityCreate
from app.database import crud_base

class EventEntityCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_event_entity(self, db: Session, event_entity: EventEntityCreate):
        event_entity_data = {
            "type": event_entity.type,
            "name": event_entity.name,
            "description": event_entity.description,
            "date": event_entity.date,
            "duration": event_entity.duration,
            "speaker_id": event_entity.speaker_id,
        }
        return self.create(db, **event_entity_data)
    
    def update_event_entity(self, db: Session, event_entity_id: int, event_entity: EventEntityCreate):
        event_entity_data = {
            "type": event_entity.type,
            "name": event_entity.name,
            "description": event_entity.description,
            "date": event_entity.date,
            "duration": event_entity.duration,
            "speaker_id": event_entity.speaker_id,
        }
        return self.update(db, event_entity_id, **event_entity_data)