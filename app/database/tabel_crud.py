from sqlalchemy.orm import Session
from app.schemas.tabel import TabelCreate
from app.database import crud_base

class TabelCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)
    
    def create_tabel(self, db: Session, tabel: TabelCreate):
        tabel_data = {
            "user_id": tabel.user_id,
            "created_at": tabel.created_at,
            "type": tabel.type,
            "leave_start_date": tabel.leave_start_date,
            "leave_end_date": tabel.leave_end_date,
        }
        return self.create(db, **tabel_data)
    
    def update_tabel(self, db: Session, tabel_id: int, tabel: TabelCreate):
        tabel_data = {
            "created_at": tabel.created_at,
            "type": tabel.type,
            "leave_start_date": tabel.leave_start_date,
            "leave_end_date": tabel.leave_end_date,
        }
        return self.update(db, tabel_id, **tabel_data)