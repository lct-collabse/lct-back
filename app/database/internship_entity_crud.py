from sqlalchemy.orm import Session
from app.schemas.internship_entity import InternshipEntityCreate
from app.database import crud_base

class InternshipEntityCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_internship_entity(self, db: Session, internship_entity: InternshipEntityCreate):
        internship_entity_data = {
            "name": internship_entity.name,
            "description": internship_entity.description,
            "required_hours": internship_entity.required_hours,
        }
        return self.create(db, **internship_entity_data)
    
    def update_internship_entity(self, db: Session, internship_entity_id: int, internship_entity: InternshipEntityCreate):
        internship_entity_data = {
            "name": internship_entity.name,
            "description": internship_entity.description,
            "required_hours": internship_entity.required_hours,
        }
        return self.update(db, internship_entity_id, **internship_entity_data)