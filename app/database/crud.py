import os
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from dotenv import load_dotenv
from app.models.entities import Base

load_dotenv()

# Получение переменных окружения
db_name = os.environ.get("DB_NAME")
db_user = os.environ.get("DB_USER")
db_password = os.environ.get("DB_PASSWORD")
db_host = os.environ.get("DB_HOST")
db_port = os.environ.get("DB_PORT")

# Формирование строки подключения к базе данных
database_url = f"postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}"
print(database_url)
engine = create_engine(database_url)
Base.metadata.create_all(bind=engine)


def get_db():
    db = Session(bind=engine)
    try:
        yield db
    finally:
        db.close()