from sqlalchemy.orm import Session

class CRUDBase:
    def __init__(self, model):
        self.model = model

    def get_all(self, db: Session):
        return db.query(self.model).all()
    
    def create(self, db: Session, **kwargs):
        existing_obj = db.query(self.model).filter_by(**kwargs).first()
        if existing_obj:
            # Handle duplicate object case
            return None

        db_obj = self.model(**kwargs)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get(self, db: Session, id):
        print(self.model)
        return db.query(self.model).filter(self.model.id == id).first()

    def update(self, db: Session, id, obj):
        db_obj = db.query(self.model).filter(self.model.id == id).first()
        for attr, value in obj.dict().items():
            setattr(db_obj, attr, value)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def delete(self, db: Session, id):
        db_obj = db.query(self.model).filter(self.model.id == id).first()
        db.delete(db_obj)
        db.commit()
        return {"message": "Entity deleted successfully"}
