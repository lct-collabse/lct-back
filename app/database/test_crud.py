from sqlalchemy.orm import Session
from app.schemas.test import TestCreate
from app.database import crud_base

class TabelCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_test(self, db: Session, test: TestCreate):
        test_data = {
            "description": test.description,
            "internship_id": test.internship_id,
        }
        return self.create(db, **test_data)
    
    def update_test(self, db: Session, test_id: int, test: TestCreate):
        test_data = {
            "description": test.description,
            "internship_id": test.internship_id,
        }
        return self.update(db, test_id, **test_data)