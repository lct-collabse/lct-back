from sqlalchemy.orm import Session
from app.schemas.ml_internship_recomendation import ML_InternshipRecomendationCreate
from app.database import crud_base

class InternshipRecomendationCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_internship_recomendation(self, db: Session, internship_recomendation: ML_InternshipRecomendationCreate, user_id: int):
        event_recomendation_data = {
            "user_id": internship_recomendation.user_id,
            "internship_id": internship_recomendation.internship_id,
            "value": internship_recomendation.value,
            "date": internship_recomendation.date
        }
        return self.create(db, **event_recomendation_data)
    
    def update_internship_recomendation(self, db: Session, user_id: int, internship_recomendation_id: int, internship_recomendation: ML_InternshipRecomendationCreate):
        event_recomendation_data = {
            "user_id": user_id,
            "internship_id": internship_recomendation.internship_id,
            "value": internship_recomendation.value,
            "date": internship_recomendation.date
        }
        return self.update(db, id=internship_recomendation_id, obj_in=event_recomendation_data)