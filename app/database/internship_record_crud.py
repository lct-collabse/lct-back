from sqlalchemy.orm import Session
from app.schemas.internship_record import InternshipRecordCreate
from app.database import crud_base

class InternshipRecordCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_internship_record(self, db: Session, internship_record: InternshipRecordCreate):
        internship_record_data = {
            "student_id": internship_record.student_id,
            "mentor_id": internship_record.mentor_id,
            "curator_id": internship_record.curator_id,
            "request_id": internship_record.request_id,
            "status": internship_record.status,
            "points": internship_record.points,
        }
        return self.create(db, **internship_record_data)
    
    def update_internship_record(self, db: Session, internship_record_id: int, internship_record: InternshipRecordCreate):
        internship_record_data = {
            "student_id": internship_record.student_id,
            "mentor_id": internship_record.mentor_id,
            "curator_id": internship_record.curator_id,
            "request_id": internship_record.request_id,
            "status": internship_record.status,
            "points": internship_record.points,
        }
        return self.update(db, internship_record_id, **internship_record_data)