from sqlalchemy.orm import Session
from app.schemas.motivation import MotivationCreate
from app.database.crud_base import CRUDBase

class MotivationCRUD(CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_motivation(self, db: Session, motivation: MotivationCreate, user_id: int):
        motivation_data = {
            "user_id": user_id,
            "author_id": motivation.author_id,
            "type": motivation.type,
            "name": motivation.name,
            "description": motivation.description,
            "start_date": motivation.start_date,
            "end_date": motivation.end_date,
            "status": motivation.status,
        }
        return self.create(db, **motivation_data)
    
    def update_motivation(self, db: Session, motivation_id: int, motivation: MotivationCreate):
        motivation_data = {
            "type": motivation.type,
            "name": motivation.name,
            "description": motivation.description,
            "start_date": motivation.start_date,
            "end_date": motivation.end_date,
            "status": motivation.status,
        }
        return self.update(db, motivation_id, **motivation_data)