from sqlalchemy.orm import Session
from app.schemas.ml_event_recomendation import ML_EventRecomendationCreate
from app.database import crud_base

class EventRecomendationCRUD(crud_base.CRUDBase):
    def __init__(self, model):
        super().__init__(model)

    def create_event_recomendation(self, db: Session, event_recomendation: ML_EventRecomendationCreate):
        event_recomendation_data = {
            "user_id": event_recomendation.user_id,
            "event_id": event_recomendation.event_id,
            "value": event_recomendation.value,
            "date": event_recomendation.date
        }
        return self.create(db, **event_recomendation_data)
    
    def get_event_recomendation_by_user_id(self, db: Session, user_id: int):
        return db.query(self.model).filter(self.model.user_id == user_id).all()
    
    def get_event_recomendation_by_user_id_and_event_id(self, db: Session, user_id: int, event_id: int):
        return db.query(self.model).filter(self.model.user_id == user_id).filter(self.model.event_id == event_id).first()
    
    def update_event_recomendation(self, db: Session, user_id: int, event_id: int, event_recomendation: ML_EventRecomendationCreate):
        event_recomendation_data = {
            "user_id": user_id,
            "event_id": event_id,
            "value": event_recomendation.value,
            "date": event_recomendation.date
        }
        return self.update(db, **event_recomendation_data)