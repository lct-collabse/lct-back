from sqlalchemy.orm import Session
from app.schemas.university import UniversityCreate
from app.database.crud_base import CRUDBase

class UniversityCRUD(CRUDBase):
    def __init__(self, model):
        super().__init__(model)
    
    def create_university(self, db: Session, university: UniversityCreate):
        university_data = {
            "name": university.name,
            "city": university.city,
        }
        return self.create(db, **university_data)
    
    def update_university(self, db: Session, university_id: int, university: UniversityCreate):
        university_data = {
            "name": university.name,
            "city": university.city,
        }
        return self.update(db, university_id, **university_data)