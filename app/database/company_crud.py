from sqlalchemy.orm import Session
from app.schemas.company import CompanyCreate
from app.database.crud_base import CRUDBase

class CompanyCRUD(CRUDBase):
    def __init__(self, model):
        super().__init__(model)
    
    def create_company(self, db: Session, company: CompanyCreate):
        company_data = {
            "name": company.name,
            "description": company.description,
        }
        return self.create(db, **company_data)
    
    def update_company(self, db: Session, company_id: int, company: CompanyCreate):
        company_data = {
            "name": company.name,
            "description": company.description,
        }
        return self.update(db, company_id, **company_data)