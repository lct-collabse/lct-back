from fastapi import FastAPI
from app.routers.crud import user, review, motivation, university, education, company, tabel, work, internship_entity, internship_record, test, event_entity, event_record, ml_internship_recomendation, ml_event_recomendation

app = FastAPI()

app.include_router(user.router)
app.include_router(review.router)
app.include_router(motivation.router)
app.include_router(university.router)
app.include_router(education.router)
app.include_router(company.router)
app.include_router(tabel.router)
app.include_router(work.router)
app.include_router(internship_entity.router)
app.include_router(internship_record.router)
app.include_router(test.router)
app.include_router(event_entity.router)
app.include_router(event_record.router)
app.include_router(ml_internship_recomendation.router)
app.include_router(ml_event_recomendation.router)
app.include_router(ml_event_recomendation.router)