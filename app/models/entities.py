from sqlalchemy import Column, Integer, String, ForeignKey, Date
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = "User"

    id = Column(Integer, primary_key=True)
    first_name = Column(String(255), nullable=False)
    middle_name = Column(String(255), nullable=True)
    last_name = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False)
    password = Column(String(1500), nullable=False)
    role = Column(String(255), nullable=False)
    vk_id = Column(String(255), nullable=True)
    tg_id = Column(String(255), nullable=True)
    phone_number = Column(String(255), nullable=True)

    reviews = relationship("Review", back_populates="author")
    motivations = relationship("Motivation", back_populates="user")
    works = relationship("Work", back_populates="user")
    educations = relationship("Education", back_populates="user")
    events_speaker = relationship("EventEntity", back_populates="speaker")
    events_attended = relationship("EventRecord", back_populates="participant")
    event_recomendations = relationship("ML_EventRecomendation", back_populates="user")
    tabels = relationship("Tabel", back_populates="user")
    
    students_internships = relationship("InternshipRecord", backref="student", foreign_keys="[InternshipRecord.student_id]")
    mentor_internships = relationship("InternshipRecord", backref="mentor", foreign_keys="[InternshipRecord.mentor_id]")
    curator_internships = relationship("InternshipRecord", backref="curator", foreign_keys="[InternshipRecord.curator_id]")
    internship_recomendations = relationship("ML_InternshipRecomendation", back_populates="user")

class Review(Base):
    __tablename__ = "Review"

    id = Column(Integer, primary_key=True)
    author_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    target_id = Column(Integer, nullable=False)
    target_type = Column(String(255), nullable=False)
    name = Column(String(255), nullable=False)
    description = Column(String(1000), nullable=False)
    grade = Column(Integer, nullable=False)

    author = relationship("User", back_populates="reviews")

class Motivation(Base):
    __tablename__ = "Motivation"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    type = Column(String(255), nullable=False)
    name = Column(String(255), nullable=False)
    description = Column(String(255), nullable=False)
    start_date = Column(Date)
    end_date = Column(Date)
    status = Column(String(255), nullable=False)

    user = relationship("User", back_populates="motivations")

class Company(Base):
    __tablename__ = "Company"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), nullable=False)
    description = Column(String(1000), nullable=False)

    works = relationship("Work", back_populates="company")

class University(Base):
    __tablename__ = "University"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), nullable=False)
    city = Column(String(255), nullable=False)

    educations = relationship("Education", back_populates="university")

class Work(Base):
    __tablename__ = "Work"

    id = Column(Integer,

 primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    company_id = Column(Integer, ForeignKey("Company.id"), nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
    job_title = Column(String(255), nullable=False)
    job_description = Column(String(1000), nullable=False)

    user = relationship("User", back_populates="works")
    company = relationship("Company", back_populates="works")

class Education(Base):
    __tablename__ = "Education"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    university_id = Column(Integer, ForeignKey("University.id"), nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
    speciality_title = Column(String(255), nullable=False)
    education_level = Column(String(255), nullable=False)

    user = relationship("User", back_populates="educations")
    university = relationship("University", back_populates="educations")

class EventEntity(Base):
    __tablename__ = "EventEntity"

    id = Column(Integer, primary_key=True, index=True)
    speaker_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    type = Column(String(255), nullable=False)
    name = Column(String(255), nullable=False)
    description = Column(String(255), nullable=False)
    date = Column(Date, nullable=False)
    duration = Column(Integer, nullable=False)

    speaker = relationship("User", back_populates="events_speaker")
    event_occurences = relationship("EventRecord", back_populates="event")
    recomendations = relationship("ML_EventRecomendation", back_populates="event")

class EventRecord(Base):
    __tablename__ = "EventRecord"

    id = Column(Integer, primary_key=True, index=True)
    participant_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    event_id = Column(Integer, ForeignKey("EventEntity.id"), nullable=False)
    internship_id = Column(Integer, ForeignKey("InternshipEntity.id"), nullable=False)

    participant = relationship("User", back_populates="events_attended")
    event = relationship("EventEntity", back_populates="event_occurences")
    # internship_link = relationship("InternshipEntity", back_populates="internship_events")

class ML_EventRecomendation(Base):
    __tablename__ = "ML_EventRecomendation"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    event_id = Column(Integer, ForeignKey("EventEntity.id"), nullable=False)
    value = Column(Integer, nullable=False)
    date = Column(Date, nullable=False)

    user = relationship("User", back_populates="event_recomendations")
    event = relationship("EventEntity", back_populates="recomendations")

class Tabel(Base):
    __tablename__ = "Tabel"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    created_at = Column(Date, nullable=False)
    type = Column(String(255), nullable=False)
    leave_start_date = Column(Date, nullable=False)
    leave_end_date = Column(Date, nullable=False)

    user = relationship("User", back_populates="tabels")

class InternshipEntity(Base):
    __tablename__ = "InternshipEntity"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), nullable=False)
    description = Column(String(

1000), nullable=False)
    required_hours = Column(Integer, nullable=False)

    internship_records = relationship("InternshipRecord", backref="internship_entity")
    internship_events = relationship("EventRecord", backref="internship_link")
    tests = relationship("Test", backref="internship_testing")
    internship_recomendations = relationship("ML_InternshipRecomendation", back_populates="internship")

class InternshipRecord(Base):
    __tablename__ = "InternshipRecord"

    id = Column(Integer, primary_key=True, index=True)
    student_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    mentor_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    curator_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    request_id = Column(Integer, ForeignKey("InternshipEntity.id"), nullable=False)
    status = Column(String(255), nullable=False)
    points = Column(Integer, nullable=False)

    # student_user = relationship("User", backref="students_internships", foreign_keys=[student_id])
    # mentor = relationship("User", backref="mentor_internships", foreign_keys=[mentor_id])
    # curator = relationship("User", backref="curator_internships", foreign_keys=[curator_id])
    # internship_entity = relationship("InternshipEntity", backref="internship_records")

class Test(Base):
    __tablename__ = "Test"

    id = Column(Integer, primary_key=True, index=True)
    internship_id = Column(Integer, ForeignKey("InternshipEntity.id"), nullable=False)
    description = Column(String(1000), nullable=False)

    # internship_testing = relationship("InternshipEntity", backref="tests")

class ML_InternshipRecomendation(Base):
    __tablename__ = "ML_InternshipRecomendation"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("User.id"), nullable=False)
    internship_id = Column(Integer, ForeignKey("InternshipEntity.id"), nullable=False)
    value = Column(Integer, nullable=False)
    date = Column(Date, nullable=False)

    user = relationship("User", back_populates="internship_recomendations")
    internship = relationship("InternshipEntity", back_populates="internship_recomendations")