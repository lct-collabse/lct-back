from typing import Optional
from pydantic import BaseModel

class ReviewBase(BaseModel):
    target_id: int
    target_type: str
    name: str
    description: str
    grade: int
    author_id: int

class ReviewCreate(ReviewBase):
    pass

class ReviewResponseModel(ReviewBase):
    id: int

    class Config:
        orm_mode = True

