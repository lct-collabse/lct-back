from pydantic import BaseModel

class EventRecordBase(BaseModel):
    participant_id: int
    event_id: int
    internship_id: int

class EventRecordCreate(EventRecordBase):
    pass

class EventRecordUpdate(EventRecordBase):
    pass

class EventRecordResponse(EventRecordBase):
    id: int

    class Config:
        orm_mode = True
