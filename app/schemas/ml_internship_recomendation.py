from pydantic import BaseModel
from datetime import date

class ML_InternshipRecomendationBase(BaseModel):
    value: int
    date: date
    user_id: int
    internship_id: int

class ML_InternshipRecomendationCreate(ML_InternshipRecomendationBase):
    pass

class ML_InternshipRecomendationUpdate(ML_InternshipRecomendationBase):
    pass

class ML_InternshipRecomendationResponse(ML_InternshipRecomendationBase):
    id: int

    class Config:
        orm_mode = True
