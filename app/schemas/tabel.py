from pydantic import BaseModel
from datetime import date

class TabelBase(BaseModel):
    user_id: int
    created_at: date
    type: str
    leave_start_date: date
    leave_end_date: date

class TabelCreate(TabelBase):
    pass

class TabelUpdate(TabelBase):
    pass

class TabelResponse(TabelBase):
    id: int

    class Config:
        orm_mode = True
