from pydantic import BaseModel

class InternshipRecordBase(BaseModel):
    student_id: int
    mentor_id: int
    curator_id: int
    request_id: int
    status: str
    points: int

class InternshipRecordCreate(InternshipRecordBase):
    pass

class InternshipRecordUpdate(InternshipRecordBase):
    pass

class InternshipRecordResponse(InternshipRecordBase):
    id: int

    class Config:
        orm_mode = True
