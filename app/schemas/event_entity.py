from pydantic import BaseModel
from datetime import date

class EventEntityBase(BaseModel):
    type: str
    name: str
    description: str
    date: date
    duration: int
    speaker_id: int

class EventEntityCreate(EventEntityBase):
    pass

class EventEntityUpdate(EventEntityBase):
    pass

class EventEntityResponse(EventEntityBase):
    id: int

    class Config:
        orm_mode = True
