from pydantic import BaseModel

class InternshipEntityBase(BaseModel):
    name: str
    description: str
    required_hours: int

class InternshipEntityCreate(InternshipEntityBase):
    pass

class InternshipEntityUpdate(InternshipEntityBase):
    pass

class InternshipEntityResponse(InternshipEntityBase):
    id: int

    class Config:
        orm_mode = True
