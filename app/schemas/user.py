from pydantic import BaseModel
from typing import Optional

class UserBase(BaseModel):
    first_name: str
    middle_name: str
    last_name: str
    email: str
    password: str
    vk_id: Optional[str]
    tg_id: Optional[str]
    phone_number: Optional[str]
    role: str

class UserCreate(UserBase):
    pass

class UserResponseModel(UserBase):
    id: int

    class Config:
        orm_mode = True