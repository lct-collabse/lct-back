from pydantic import BaseModel

class UniversityBase(BaseModel):
    name: str
    city: str

class UniversityCreate(UniversityBase):
    pass

class UniversityUpdate(UniversityBase):
    pass

class UniversityResponse(UniversityBase):
    id: int

    class Config:
        orm_mode = True
