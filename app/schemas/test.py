from pydantic import BaseModel

class TestBase(BaseModel):
    description: str
    internship_id: int

class TestCreate(TestBase):
    pass

class TestUpdate(TestBase):
    pass

class TestResponse(TestBase):
    id: int

    class Config:
        orm_mode = True
