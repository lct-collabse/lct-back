from pydantic import BaseModel
from datetime import date
from typing import Optional

class WorkBase(BaseModel):
    start_date: date
    end_date: Optional[date]
    job_title: str
    job_description: str
    user_id: int
    company_id: int

class WorkCreate(WorkBase):
    pass

class WorkUpdate(WorkBase):
    pass

class WorkResponse(WorkBase):
    id: int

    class Config:
        orm_mode = True
