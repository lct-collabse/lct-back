from pydantic import BaseModel
from datetime import date
from typing import Optional

class EducationBase(BaseModel):
    start_date: date
    end_date: Optional[date]
    speciality_title: str
    education_level: str
    user_id: int
    university_id: int

class EducationCreate(EducationBase):
    pass

class EducationUpdate(EducationBase):
    pass

class EducationResponse(EducationBase):
    id: int

    class Config:
        orm_mode = True
