from pydantic import BaseModel
import datetime

class MotivationBase(BaseModel):
    type: str
    name: str
    description: str
    start_date: datetime.date
    end_date: datetime.date
    status: str
    user_id: int

class MotivationCreate(MotivationBase):
    pass

class MotivationUpdate(MotivationBase):
    pass

class MotivationResponseModel(MotivationBase):
    id: int

    class Config:
        orm_mode = True
