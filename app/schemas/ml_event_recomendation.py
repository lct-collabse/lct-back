from pydantic import BaseModel
from datetime import date

class ML_EventRecomendationBase(BaseModel):
    user_id: int
    event_id: int
    value: int
    date: date

class ML_EventRecomendationCreate(ML_EventRecomendationBase):
    pass

class ML_EventRecomendationUpdate(ML_EventRecomendationBase):
    pass

class ML_EventRecomendationResponse(ML_EventRecomendationBase):
    id: int

    class Config:
        orm_mode = True
