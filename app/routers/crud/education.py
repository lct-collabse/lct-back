from fastapi import APIRouter, Depends, HTTPException
from app.schemas.education import EducationCreate, EducationResponse
from app.database import crud
from app.database.education_crud import EducationCRUD
from sqlalchemy.orm import Session
from app.models.entities import Education
from typing import List
from app.routers import user, university

router = APIRouter(tags=["Education"])
target_crud = EducationCRUD(Education)

@router.get("/education", response_model=List[EducationResponse])
def get_all_education(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/education/", response_model=EducationResponse, tags=["Education"], status_code=201)
def create_education(education: EducationCreate, db: Session = Depends(crud.get_db)):
    if education.user_id is None:
        raise HTTPException(status_code=400, detail="User ID must not be null")
    if user.get(db, education.user_id) is None:
        raise HTTPException(status_code=404, detail="User not found")

    if education.university_id is None:
        raise HTTPException(status_code=400, detail="University ID must not be null")
    if university.get(db, education.university_id) is None:
        raise HTTPException(status_code=404, detail="University not found")

    result = target_crud.create_education(db, education)
    return result

@router.get("/education/{education_id}", response_model=EducationResponse, tags=["Education"], status_code=200)
def read_education(education_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, education_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Education not found")
    return result

@router.put("/education/{education_id}", response_model=EducationResponse, tags=["Education"], status_code=200)
def update_education(education_id: int, education: EducationCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_education(education_id, education, db)

@router.delete("/education/{education_id}", tags=["Education"], status_code=204)
def delete_education(education_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, education_id)