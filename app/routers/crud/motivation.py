from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.database.crud import get_db
from app.database.motivation_crud import MotivationCRUD
from app.models.entities import Motivation
from app.schemas.motivation import MotivationCreate, MotivationResponseModel
from app.routers.user import read_user
from typing import List

router = APIRouter(tags=["Motivation"])
target_crud = MotivationCRUD(Motivation)

@router.get("/motivations", response_model=List[MotivationResponseModel], tags=["Motivation"])
def get_all_motivations(db: Session = Depends(get_db)):
    return target_crud.get_all(db)

@router.post("/motivations", response_model=MotivationResponseModel, status_code=201, tags=["Motivation"])
def create_user_motivation(motivation: MotivationCreate, db: Session = Depends(get_db)):
    if motivation.author_id is None:
        raise HTTPException(status_code=400, detail="Author ID is required")
    if read_user(motivation.author_id, db) is None:
        raise HTTPException(status_code=404, detail="Author not found")
    return target_crud.create_motivation(db, motivation)

@router.get("/motivations/{motivation_id}", response_model=MotivationResponseModel, tags=["Motivation"])
def read_user_motivation(motivation_id: int, db: Session = Depends(get_db)):
    return target_crud.get(db, motivation_id)

@router.put("/motivations/{motivation_id}", response_model=MotivationResponseModel, tags=["Motivation"])
def update_user_motivation(motivation_id: int, motivation: MotivationCreate, db: Session = Depends(get_db)):
    return target_crud.update_motivation(motivation_id, motivation, db)

@router.delete("/motivations/{motivation_id}", tags=["Motivation"])
def delete_user_motivation(motivation_id: int, db: Session = Depends(get_db)):
    return target_crud.delete(db, motivation_id)
