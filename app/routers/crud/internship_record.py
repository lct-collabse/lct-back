from fastapi import APIRouter, Depends, HTTPException
from app.schemas.internship_record import InternshipRecordCreate, InternshipRecordResponse
from app.database import crud
from app.database.internship_record_crud import InternshipRecordCRUD
from sqlalchemy.orm import Session
from app.models.entities import InternshipRecord
from typing import List
from app.routers import user

router = APIRouter(tags=["Internship.Record"])
target_crud = InternshipRecordCRUD(InternshipRecord)

@router.get("/internship/record", response_model=List[InternshipRecordResponse], tags=["Internship.Record"])
def get_all_internship_record(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/internship/record/", response_model=InternshipRecordResponse, tags=["Internship.Record"], status_code=201)
def create_internship_record(internship_record: InternshipRecordCreate, db: Session = Depends(crud.get_db)):
    if internship_record.user_id is None:
        raise HTTPException(status_code=400, detail="User ID is required")
    if internship_record.curator_id is None:
        raise HTTPException(status_code=400, detail="Curator ID is required")
    if internship_record.mentor_id is None:
        raise HTTPException(status_code=400, detail="Mentor ID is required")
    
    if user.read_user(internship_record.user_id, db) is None:
        raise HTTPException(status_code=404, detail="User not found")
    if user.read_user(internship_record.curator_id, db) is None:
        raise HTTPException(status_code=404, detail="Curator not found")
    if user.read_user(internship_record.mentor_id, db) is None:
        raise HTTPException(status_code=404, detail="Mentor not found")

    result = target_crud.create_internship_record(db, internship_record)
    return result

@router.get("/internship/record/{internship_record_id}", response_model=InternshipRecordResponse, tags=["Internship.Record"], status_code=200)
def read_internship_record(internship_record_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, internship_record_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Internship record not found")
    return result

@router.put("/internship/record/{internship_record_id}", response_model=InternshipRecordResponse, tags=["Internship.Record"], status_code=200)
def update_internship_record(internship_record_id: int, internship_record: InternshipRecordCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_internship_record(internship_record_id, internship_record, db)

@router.delete("/internship/record/{internship_record_id}", tags=["Internship.Record"], status_code=204)
def delete_internship_record(internship_record_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, internship_record_id)
