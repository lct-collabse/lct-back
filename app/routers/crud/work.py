from fastapi import APIRouter, Depends, HTTPException
from app.schemas.work import WorkResponse, WorkCreate
from app.database import crud
from app.database.work_crud import WorkCRUD
from sqlalchemy.orm import Session
from app.models.entities import Work
from typing import List

router = APIRouter(tags=["Works"])
target_crud = WorkCRUD(Work)

@router.get("/works", response_model=List[WorkResponse])
def get_all_works(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/works/", response_model=WorkResponse, tags=["Works"], status_code=201)
def create_work(work: WorkCreate, db: Session = Depends(crud.get_db)):
    if crud.read_user(work.user_id, db) is None:
        raise HTTPException(status_code=404, detail="User not found")
    if crud.read_company(work.company_id, db) is None:
        raise HTTPException(status_code=404, detail="Company not found")
    
    result = target_crud.create_work(db, work)
    return result

@router.get("/works/{work_id}", response_model=WorkResponse, tags=["Works"], status_code=200)
def read_work(work_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, work_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Work not found")
    return result

@router.put("/works/{work_id}", response_model=WorkResponse, tags=["Works"], status_code=200)
def update_work(work_id: int, work: WorkCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_work(work_id, work, db)

@router.delete("/works/{work_id}", tags=["Works"], status_code=204)
def delete_work(work_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, work_id)