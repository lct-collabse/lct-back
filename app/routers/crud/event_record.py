from fastapi import APIRouter, Depends, HTTPException
from app.schemas.event_record import EventRecordCreate, EventRecordResponse
from app.database import crud
from app.database.event_record_crud import EventRecordCRUD
from sqlalchemy.orm import Session
from app.models.entities import EventRecord
from typing import List
from app.routers import event_entity, user, internship_entity

router = APIRouter(tags=["Event.Record"])
target_crud = EventRecordCRUD(EventRecord)

@router.get("/events/record", response_model=List[EventRecordResponse], tags=["Event.Record"])
def get_all_events(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/events/record/", response_model=EventRecordResponse, tags=["Event.Record"], status_code=201)
def create_event(event: EventRecordCreate, db: Session = Depends(crud.get_db)):
    if event.event_id is not None:
        raise HTTPException(status_code=400, detail="Event ID must be null")
    if event_entity.get(db, event.event_id) is None:
        raise HTTPException(status_code=404, detail="Event not found")
    
    if event.participant_id is None:
        raise HTTPException(status_code=400, detail="Participant ID must not be null")
    if user.get(db, event.participant_id) is None:
        raise HTTPException(status_code=404, detail="Participant not found")
    
    if event.internship_id is None:
        raise HTTPException(status_code=400, detail="Internship ID must not be null")
    if internship_entity.get(db, event.internship_id) is None:
        raise HTTPException(status_code=404, detail="Internship not found")

    result = target_crud.create_event_record(db, event)
    return result

@router.get("/events/record/{event_id}", response_model=EventRecordResponse, tags=["Event.Record"], status_code=200)
def read_event(event_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, event_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Event not found")
    return result

@router.put("/events/record/{event_id}", response_model=EventRecordResponse, tags=["Event.Record"], status_code=200)
def update_event(event_id: int, event: EventRecordCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_event_record(db, event_id, event)

@router.delete("/events/record/{event_id}", tags=["Event.Record"], status_code=204)
def delete_event(event_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, event_id)