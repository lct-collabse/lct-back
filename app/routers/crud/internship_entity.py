from fastapi import APIRouter, Depends, HTTPException
from app.schemas.internship_entity import InternshipEntityCreate, InternshipEntityResponse
from app.database import crud
from app.database.internship_entity_crud import InternshipEntityCRUD
from sqlalchemy.orm import Session
from app.models.entities import InternshipEntity
from typing import List

router = APIRouter(tags=["Internship.Request"])
target_crud = InternshipEntityCRUD(InternshipEntity)

@router.get("/internship/request", response_model=List[InternshipEntityResponse], tags=["Internship.Request"])
def get_all_internship_request(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/internship/request/", response_model=InternshipEntityResponse, tags=["Internship.Request"], status_code=201)
def create_internship_request(internship_request: InternshipEntityCreate, db: Session = Depends(crud.get_db)):
    result = target_crud.create_internship_entity(db, internship_request)
    return result

@router.get("/internship/request/{internship_request_id}", response_model=InternshipEntityResponse, tags=["Internship.Request"], status_code=200)
def read_internship_request(internship_request_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, internship_request_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Internship request not found")
    return result

@router.put("/internship/request/{internship_request_id}", response_model=InternshipEntityResponse, tags=["Internship.Request"], status_code=200)
def update_internship_request(internship_request_id: int, internship_request: InternshipEntityCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_internship_entity(internship_request_id, internship_request, db)

@router.delete("/internship/request/{internship_request_id}", tags=["Internship.Request"], status_code=204)
def delete_internship_request(internship_request_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, internship_request_id)