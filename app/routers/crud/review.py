from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.database.crud import get_db
from app.database.review_crud import ReviewCRUD
from app.models.entities import Review
from app.schemas.review import ReviewCreate, ReviewResponseModel
from app.routers.user import read_user
from typing import List

router = APIRouter(tags=["Reviews"])
target_crud = ReviewCRUD(Review)

@router.get("/reviews", response_model=List[ReviewResponseModel], tags=["Reviews"])
def get_all_reviews(db: Session = Depends(get_db)):
    return target_crud.get_all(db)

@router.post("/reviews", response_model=ReviewResponseModel, status_code=201, tags=["Reviews"])
def create_review(review: ReviewCreate, db: Session = Depends(get_db)):
    if read_user(review.author_id, db) is None:
        raise HTTPException(status_code=404, detail="Author not found")
    return target_crud.create_review(db, review)

@router.get("/reviews/{review_id}", response_model=ReviewResponseModel, tags=["Reviews"])
def read_review(review_id: int, db: Session = Depends(get_db)):
    result = target_crud.get(db, review_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Review not found")
    return result

@router.put("/reviews/{review_id}", response_model=ReviewResponseModel, tags=["Reviews"])
def update_review(review_id: int, review: ReviewCreate, db: Session = Depends(get_db)):
    return target_crud.update_review(review_id, review, db)

@router.delete("/reviews/{review_id}", tags=["Reviews"])
def delete_review(review_id: int, db: Session = Depends(get_db)):
    return target_crud.delete(db, review_id)
