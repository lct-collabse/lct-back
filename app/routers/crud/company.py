from fastapi import APIRouter, Depends, HTTPException
from app.schemas.company import CompanyCreate, CompanyResponse
from app.database import crud
from app.database.company_crud import CompanyCRUD
from sqlalchemy.orm import Session
from app.models.entities import Company
from typing import List

router = APIRouter(tags=["Companies"])
target_crud = CompanyCRUD(Company)

@router.get("/companies", response_model=List[CompanyResponse])
def get_all_companies(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/companies/", response_model=CompanyResponse, tags=["Companies"], status_code=201)
def create_company(company: CompanyCreate, db: Session = Depends(crud.get_db)):
    result = target_crud.create_company(db, company)
    return result

@router.get("/companies/{company_id}", response_model=CompanyResponse, tags=["Companies"], status_code=200)
def read_company(company_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, company_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Company not found")
    return result

@router.put("/companies/{company_id}", response_model=CompanyResponse, tags=["Companies"], status_code=200)
def update_company(company_id: int, company: CompanyCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_company(company_id, company, db)

@router.delete("/companies/{company_id}", tags=["Companies"], status_code=204)
def delete_company(company_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, company_id)