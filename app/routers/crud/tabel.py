from fastapi import APIRouter, Depends, HTTPException
from app.schemas.tabel import TabelCreate, TabelResponse
from app.database import crud
from app.database.tabel_crud import TabelCRUD
from sqlalchemy.orm import Session
from app.models.entities import Tabel
from typing import List
from app.routers import user

router = APIRouter(tags=["Tabels"])
target_crud = TabelCRUD(Tabel)

@router.get("/tabels", response_model=List[TabelResponse])
def get_all_companies(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/tabels/", response_model=TabelResponse, tags=["Tabels"], status_code=201)
def create_tabel(tabel: TabelCreate, db: Session = Depends(crud.get_db)):
    if tabel.user_id is None:
        raise HTTPException(status_code=400, detail="User ID is required")
    if user.get_user(db, tabel.user_id) is None:
        raise HTTPException(status_code=404, detail="User is not found")

    result = target_crud.create_tabel(db, tabel)
    return result

@router.get("/tabels/{tabel_id}", response_model=TabelResponse, tags=["Tabels"], status_code=200)
def read_tabel(tabel_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, tabel_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Tabel is not found")
    return result

@router.put("/tabels/{tabel_id}", response_model=TabelResponse, tags=["Tabels"], status_code=200)
def update_tabel(tabel_id: int, tabel: TabelCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_tabel(tabel_id, tabel, db)

@router.delete("/tabels/{tabel_id}", tags=["Tabels"], status_code=204)
def delete_tabel(tabel_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, tabel_id)