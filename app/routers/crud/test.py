from fastapi import APIRouter, Depends, HTTPException
from app.schemas.test import TestCreate, TestResponse
from app.database import crud
from app.database.test_crud import TabelCRUD
from sqlalchemy.orm import Session
from app.models.entities import Test
from typing import List
from app.routers import internship_entity

router = APIRouter(tags=["Tests"])
target_crud = TabelCRUD(Test)

@router.get("/tests", response_model=List[TestResponse])
def get_all_tests(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/tests/", response_model=TestResponse, tags=["Tests"], status_code=201)
def create_test(test: TestCreate, db: Session = Depends(crud.get_db)):
    if test.internship_id is None:
        raise HTTPException(status_code=400, detail="Internship ID is required")
    if internship_entity.read_internship_request(db, test.internship_id) is None:
        raise HTTPException(status_code=404, detail="Internship is not found")

    result = target_crud.create_test(db, test)
    return result

@router.get("/tests/{tabel_id}", response_model=TestResponse, tags=["Tests"], status_code=200)
def read_test(tabel_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, tabel_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Tabel is not found")
    return result

@router.put("/tests/{tabel_id}", response_model=TestResponse, tags=["Tests"], status_code=200)
def update_test(tabel_id: int, test: TestCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_test(tabel_id, test, db)

@router.delete("/tests/{tabel_id}", tags=["Tests"], status_code=204)
def delete_test(tabel_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, tabel_id)