from fastapi import APIRouter, Depends, HTTPException
from app.schemas.user import UserResponseModel, UserCreate
from app.database import crud
from app.database.user_crud import UserCRUD
from sqlalchemy.orm import Session
from app.models.entities import User
from typing import List
from flask_bcrypt import Bcrypt
from app.services.auth_service import AuthService
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

router = APIRouter(tags=["Users"])
target_crud = UserCRUD(User)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login")

secret_key = "ad40b283aeb2300f7ef80f54ebcbc1535a509253d57dd5fb5eb0fe78cfdfa784"
algorithm = "HS256"
access_token_expire_minutes = 30

auth_service = AuthService(secret_key, algorithm, access_token_expire_minutes)

def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(crud.get_db)):
    email = auth_service.decode_access_token(token)
    user = read_user_by_email(email, db)
    if not user:
        raise HTTPException(status_code=401, detail="Invalid token")
    return user

def is_curator(user: User = Depends(get_current_user)):
    if user.role != "Curator":
        raise HTTPException(status_code=403, detail="Admin access required")
    
def is_mentor(user: User = Depends(get_current_user)):
    if user.role != "Mentor":
        raise HTTPException(status_code=403, detail="Mentor access required")

def is_hr(user: User = Depends(get_current_user)):
    if user.role != "HR":
        raise HTTPException(status_code=403, detail="HR access required")

@router.post("/login/", status_code=200)
def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(crud.get_db)):    
    email = form_data.username
    password = form_data.password
    
    return auth_service.login(email, password, db)

@router.post("/signup/", response_model=UserResponseModel, status_code=201)
def create_user(user: UserCreate, db: Session = Depends(crud.get_db)):
    user.password = Bcrypt().generate_password_hash(user.password).decode('utf-8')
    result = target_crud.create_user(db, user)
    return result

@router.get("/users", response_model=List[UserResponseModel])
def get_all_users(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.get("/users?email={email}", response_model=UserResponseModel, tags=["Users"], status_code=200)
def read_user_by_email(email: str, db: Session = Depends(crud.get_db)):
    result = target_crud.get_by_email(db, email)
    if result is None:
        raise HTTPException(status_code=404, detail="User not found")
    return result

@router.get("/users/{user_id}", response_model=UserResponseModel, tags=["Users"], status_code=200)
def read_user(user_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, user_id)
    if result is None:
        raise HTTPException(status_code=404, detail="User not found")
    return result

@router.put("/users/{user_id}", response_model=UserResponseModel, tags=["Users"], status_code=200)
def update_user(user_id: int, user: UserCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_user(user_id, user, db)

@router.delete("/users/{user_id}", tags=["Users"], status_code=204)
def delete_user(user_id: int, db: Session = Depends(crud.get_db), user: User = Depends(is_curator)):
    return target_crud.delete(db, user_id)
