from fastapi import APIRouter, Depends, HTTPException
from app.schemas.university import UniversityCreate, UniversityResponse
from app.database import crud
from app.database.university_crud import UniversityCRUD
from sqlalchemy.orm import Session
from app.models.entities import University
from typing import List

router = APIRouter(tags=["Universities"])
target_crud = UniversityCRUD(University)

@router.get("/universities", response_model=List[UniversityResponse])
def get_all_universities(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/universities/", response_model=UniversityResponse, tags=["Universities"], status_code=201)
def create_university(university: UniversityCreate, db: Session = Depends(crud.get_db)):
    result = target_crud.create_university(db, university)
    return result

@router.get("/universities/{university_id}", response_model=UniversityResponse, tags=["Universities"], status_code=200)
def read_university(university_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, university_id)
    if result is None:
        raise HTTPException(status_code=404, detail="University not found")
    return result

@router.put("/universities/{university_id}", response_model=UniversityResponse, tags=["Universities"], status_code=200)
def update_university(university_id: int, university: UniversityCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_university(university_id, university, db)

@router.delete("/universities/{university_id}", tags=["Universities"], status_code=204)
def delete_university(university_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, university_id)
