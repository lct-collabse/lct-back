from fastapi import APIRouter, Depends, HTTPException
from app.schemas.ml_internship_recomendation import ML_InternshipRecomendationCreate, ML_InternshipRecomendationResponse
from app.database import crud
from app.database.ml_internship_recomendation_crud import InternshipRecomendationCRUD
from sqlalchemy.orm import Session
from app.models.entities import ML_InternshipRecomendation
from typing import List
from app.routers import user

router = APIRouter(tags=["ML.Recomendation.Internships"])
target_crud = InternshipRecomendationCRUD(ML_InternshipRecomendation)

@router.get("/ml/recomendation/internships", response_model=List[ML_InternshipRecomendationResponse])
def get_all_internship_recomendations(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.get("/ml/recomendation/internships/{internship_recomendation_id}", response_model=ML_InternshipRecomendationResponse)
def get_internship_recomendation_by_id(internship_recomendation_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.get_by_id(db, internship_recomendation_id)

@router.post("/ml/recomendation/internships", response_model=ML_InternshipRecomendationResponse)
def create_internship_recomendation(internship_recomendation: ML_InternshipRecomendationCreate, db: Session = Depends(crud.get_db)):
    if internship_recomendation.user_id is None:
        raise HTTPException(status_code=400, detail="User ID is required")
    
    if user.read_user(internship_recomendation.user_id, db) is None:
        raise HTTPException(status_code=404, detail="User not found")

    return target_crud.create_internship_recomendation(db, internship_recomendation)

@router.put("/ml/recomendation/internships/{internship_recomendation_id}", response_model=ML_InternshipRecomendationResponse)
def update_internship_recomendation(internship_recomendation_id: int, internship_recomendation: ML_InternshipRecomendationCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_internship_recomendation(db, internship_recomendation.user_id, internship_recomendation_id, internship_recomendation)

@router.delete("/ml/recomendation/internships/{internship_recomendation_id}")
def delete_internship_recomendation(internship_recomendation_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, id=internship_recomendation_id)