from fastapi import APIRouter, Depends, HTTPException
from app.schemas.event_entity import EventEntityCreate, EventEntityResponse
from app.database import crud
from app.database.event_entity_crud import EventEntityCRUD
from sqlalchemy.orm import Session
from app.models.entities import EventEntity
from typing import List
from app.routers import user

router = APIRouter(tags=["Event.Entity"])
target_crud = EventEntityCRUD(EventEntity)

@router.get("/events/general", response_model=List[EventEntityResponse], tags=["Event.Entity"])
def get_all_events(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/events/general/", response_model=EventEntityResponse, tags=["Event.Entity"], status_code=201)
def create_event(event: EventEntityCreate, db: Session = Depends(crud.get_db)):
    if event.speaker_id is None:
        raise HTTPException(status_code=400, detail="Speaker ID must not be null")
    if user.get(db, event.speaker_id) is None:
        raise HTTPException(status_code=404, detail="Speaker not found")

    result = target_crud.create_event_entity(db, event)
    return result

@router.get("/events/general/{event_id}", response_model=EventEntityResponse, tags=["Event.Entity"], status_code=200)
def read_event(event_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, event_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Event not found")
    return result

@router.put("/events/general/{event_id}", response_model=EventEntityResponse, tags=["Event.Entity"], status_code=200)
def update_event(event_id: int, event: EventEntityCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_event_entity(db, event_id, event)

@router.delete("/events/general/{event_id}", tags=["Event.Entity"], status_code=204)
def delete_event(event_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, event_id)