from fastapi import APIRouter, Depends, HTTPException
from app.schemas.ml_event_recomendation import ML_EventRecomendationCreate, ML_EventRecomendationResponse
from app.database import crud
from app.database.ml_event_recomendation_crud import EventRecomendationCRUD
from sqlalchemy.orm import Session
from app.models.entities import ML_EventRecomendation
from typing import List
from app.routers import user, event_entity

router = APIRouter(tags=["ML.Recomendation.Event"])
target_crud = EventRecomendationCRUD(ML_EventRecomendation)

@router.get("/ml/recomendations/events", response_model=List[ML_EventRecomendationResponse], tags=["ML.Recomendation.Event"])
def get_all_events(db: Session = Depends(crud.get_db)):
    return target_crud.get_all(db)

@router.post("/ml/recomendations/events/", response_model=ML_EventRecomendationResponse, tags=["ML.Recomendation.Event"], status_code=201)
def create_event(recomendation: ML_EventRecomendationCreate, db: Session = Depends(crud.get_db)):
    if recomendation.user_id is None:
        raise HTTPException(status_code=400, detail="User ID is required")
    if user.read_user(recomendation.user_id, db) is None:
        raise HTTPException(status_code=404, detail="User not found")

    if recomendation.event_id is None:
        raise HTTPException(status_code=400, detail="Event ID is required")
    if event_entity.read_event(recomendation.event_id, db) is None:
        raise HTTPException(status_code=404, detail="Event not found")

    result = target_crud.create_event_recomendation(db, recomendation)
    return result

@router.get("/ml/recomendations/events/{event_id}", response_model=ML_EventRecomendationResponse, tags=["ML.Recomendation.Event"], status_code=200)
def read_event(event_id: int, db: Session = Depends(crud.get_db)):
    result = target_crud.get(db, event_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Event not found")
    return result

@router.put("/ml/recomendations/events/{event_id}", response_model=ML_EventRecomendationResponse, tags=["ML.Recomendation.Event"], status_code=200)
def update_event(event_id: int, user_id: int, event: ML_EventRecomendationCreate, db: Session = Depends(crud.get_db)):
    return target_crud.update_event_recomendation(db, user_id, event_id, event)

@router.delete("/ml/recomendations/events/{event_id}", tags=["ML.Recomendation.Event"], status_code=204)
def delete_event(event_id: int, db: Session = Depends(crud.get_db)):
    return target_crud.delete(db, event_id)