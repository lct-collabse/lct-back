from app.routers import user
from passlib.context import CryptContext


pwd_context_user = CryptContext(schemes=["bcrypt"], deprecated="auto")

class UserService:
    def __init__(self):
        pass

    def validate_user(self, email, password, db):
        found_user = user.target_crud.get_by_email(db, email)
        if found_user and self.verify_password(password, found_user.password):
            return found_user
        return None

    def verify_password(self, plain_password, hashed_password):
        return pwd_context_user.verify(plain_password, hashed_password)
