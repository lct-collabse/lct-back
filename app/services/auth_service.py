from datetime import datetime, timedelta
from jose import JWTError, jwt
from fastapi import HTTPException
from passlib.context import CryptContext
from app.services.user_service import UserService

pwd_context_auth = CryptContext(schemes=["bcrypt"], deprecated="auto")

user_service = UserService()

class AuthService:
    def __init__(self, secret_key, algorithm, access_token_expire_minutes):
        self.secret_key = secret_key
        self.algorithm = algorithm
        self.access_token_expire_minutes = access_token_expire_minutes

    def login(self, username, password, db):
        user = user_service.validate_user(username, password, db)
        if not user:
            raise HTTPException(status_code=401, detail="Invalid credentials")
        access_token = self.create_access_token(user.email)
        return {"access_token": access_token, "token_type": "bearer"}

    def get_password_hash(self, password):
        return pwd_context_auth.hash(password)

    def create_access_token(self, username):
        expires_delta = timedelta(minutes=self.access_token_expire_minutes)
        expire = datetime.utcnow() + expires_delta
        to_encode = {"sub": username, "exp": expire}
        encoded_jwt = jwt.encode(to_encode, self.secret_key, algorithm=self.algorithm)
        return encoded_jwt

    def decode_access_token(self, token):
        try:
            payload = jwt.decode(token, self.secret_key, algorithms=[self.algorithm])
            username = payload.get("sub")
            if username is None:
                raise HTTPException(status_code=401, detail="Invalid token")
            return username
        except JWTError:
            raise HTTPException(status_code=401, detail="Invalid token")