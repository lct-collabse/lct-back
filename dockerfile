FROM python:3.11-alpine
# 
WORKDIR /code

ARG ARG_NAME

# 
COPY ./requirements.txt /code/requirements.txt

# 
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

# 
COPY ./app /code/app

EXPOSE 8080

ENV PYTHONPATH="${PYTHONPATH}:./app"
# 
ENTRYPOINT ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8080"]